#!/usr/bin/env bash
env JEKYLL_ENV=production bundle exec jekyll build
bundle exec htmlproofer --log-level :debug ./_site
