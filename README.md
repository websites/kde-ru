# KDE.ru

This repository contains source files for the KDE.ru (KDE Russia) website.

The website is based on Jekyll, uses a superset of Markdown and some HTML includes for content, Bootstrap 4 and custom CSS for styling.

## Building

First, install the Ruby development package (e.g. `ruby-devel`), bundler (`gem install bundler`) and Ruby dependencies (`bundle install`).

To serve for testing, use:  

```sh
bundle exec jekyll serve --livereload
```

To build for production:  

```sh
JEKYLL_ENV=production bundle exec jekyll build
```

## Contributing

To contribute your changes please contact the authors by any means mentioned on the website, and notify them if you intend to commit directly.

## License

Please see [the License page](license.md) for details.
