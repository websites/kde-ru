---
title: Дистрибутивы
permalink: /distributions
virtual-parent: /get
additional-css: article
# SEO
image: img/laptop.png
description: ! 'Популярные дистрибутивы GNU/Linux с предустановленным программным обеспечением KDE'
---

# Дистрибутивы с Plasma и приложениями KDE

Дистрибутивы — это разные «комплектации» операционной системы GNU/Linux. Все они используют ядро Linux, но отличаются по предустановленному программному обеспечению, версиям компонентов, начальному внешнему виду, настройкам и другим параметрам. Выбор дистрибутива персонален и опирается на ваши предпочтения: некоторые пользователи в первую очередь ценят стабильность, другие — свежие версии программ, третьи — наличие коммерческой техподдержки, и так далее.

Ниже приведены популярные дистрибутивы с предустановленным программным обеспечением KDE. Мы рекомендуем ознакомиться с описаниями на официальных сайтах проектов, чтобы получить полное представление о них.

## Kubuntu

![Логотип Kubuntu](img/distros/logos/kubuntu.svg){: .distribution-logo}
Вариант популярного дистрибутива Ubuntu с предустановленным программным обеспечением KDE. Разработчики Kubuntu стремятся к созданию удобной для начинающих пользователей системы. В комплект включены многие востребованные программы и утилита для управления драйверами.  
*Компания Canonical, разработчик Ubuntu, — спонсор KDE.*  
[Перейти на сайт](https://kubuntu.org){: .learn-more .button .d-inline-block}  
![Скриншот Kubuntu](img/distros/bg/kubuntu.png){: .img-fluid .rounded .mb-3}

## Fedora KDE Plasma Desktop

![Логотип Fedora](img/distros/logos/fedora.svg){: .distribution-logo}
Fedora славится ранним переходом на новые технологии мира Linux. В этом варианте дистрибутива используется рабочая среда Plasma Wayland. В состав входят приложения для выхода в сеть, работы с офисными документами и мультимедиа. Есть также [версия для устройств Apple M1/M2](https://asahilinux.org/fedora/).  
[Перейти на сайт](https://fedoraproject.org/ru/spins/kde/){: .learn-more .button .d-inline-block}  
![Скриншот Fedora KDE Plasma Desktop](img/distros/bg/fedora.png){: .img-fluid .rounded .mb-3}

## openSUSE

![Логотип openSUSE](img/distros/logos/opensuse.svg){: .distribution-logo}
Дистрибутив openSUSE зарекомендовал себя строгим и основательным подходом к разработке и тестированию. Он представлен двумя вариантами:

* [Leap](https://get.opensuse.org/ru/leap/) — стабильное издание с регулярными выпусками, использующими версии компонентов с долгосрочной поддержкой (LTS).  
* [Tumbleweed](https://get.opensuse.org/ru/tumbleweed) — издание со свежими версиями всех пакетов, обновляемое непрерывно, без «новых версий системы».

*Компания SUSE — спонсор KDE.*  
[Перейти на сайт](https://www.opensuse.org){: .learn-more .button .d-inline-block}  
![Скриншот openSUSE](img/distros/bg/opensuse.png){: .img-fluid .rounded .mb-3}

## KDE Neon

![Логотип KDE Neon](img/distros/logos/neon.svg){: .distribution-logo}
Проект KDE Neon первым предоставляет самые новые версии рабочей среды и приложений KDE со стандартными настройками поверх основного дистрибутива Ubuntu с длительным сроком поддержки (LTS). Он предназначен в первую очередь для энтузиастов, которые хотят помочь с тестированием или принять участие в разработке.

Доступен в нескольких изданиях:

* для пользователей (User Edition);
* для тестировщиков (Testing Edition);
* для разработчиков (Unstable Edition и Developer Edition).

[Перейти на сайт](https://neon.kde.org){: .learn-more .button .d-inline-block}  
![Скриншот KDE Neon](img/distros/bg/neon.png){: .img-fluid .rounded .mb-3}

## Альт Рабочая станция К

![Логотип Альт Рабочая станция К](img/distros/logos/alt.svg){: .distribution-logo}
Независимый российский дистрибутив с ПО KDE, включённый в Единый реестр российских программ для ЭВМ и баз данных. Отдельно доступна [образовательная версия](https://www.basealt.ru/alt-education/description). Разработкой и поддержкой коммерческих пользователей занимается компания «Базальт СПО».  
[Перейти на сайт](https://www.basealt.ru/alt-workstation-k){: .learn-more .button .d-inline-block}  
![Скриншот Альт Рабочая станция К](img/distros/bg/alt.png){: .img-fluid .rounded .mb-3}

## Manjaro KDE

![Логотип Manjaro](img/distros/logos/manjaro.svg){: .distribution-logo}
Manjaro — это дистрибутив, основанный на [Arch Linux](https://www.archlinux.org), нацеленный на упрощение его установки и настройки. Издание Manjaro KDE, в частности, предлагает пользователям Plasma и набор основных приложений KDE.  
[Перейти на сайт](https://manjaro.org){: .learn-more .button .d-inline-block}  
![Скриншот Manjaro](img/distros/bg/manjaro.png){: .img-fluid .rounded .mb-3}

## ROSA Desktop

![Логотип ROSA](img/distros/logos/rosa.svg){: .distribution-logo}
ROSA Desktop Fresh позиционируется как дистрибутив для начинающих пользователей Linux, готовый к домашнему использованию сразу после установки. Доступна [коммерческая версия](https://rosa.ru/enterprise-chrome/). Разработкой руководит AО «НТЦ ИТ РОСА».  
[Перейти на сайт](https://rosa.ru/rosa-fresh/){: .learn-more .button .d-inline-block}  
![Скриншот ROSA Desktop Fresh](img/distros/bg/rosa.png){: .img-fluid .rounded .mb-3}

## РЕД ОС

![Логотип РЕД ОС](img/distros/logos/redos.svg){: .distribution-logo}
Российкий дистрибутив общего назначения для серверов и рабочих станций. Программное обеспечение KDE доступно в качестве варианта при установке. Разработкой руководит ООО «Ред Софт».  
[Перейти на сайт](https://redos.red-soft.ru/product/station/){: .learn-more .button .d-inline-block}  
![Скриншот РЕД ОС](img/distros/bg/redos.png){: .img-fluid .rounded .mb-3}

## Другие дистрибутивы

Многие другие дистрибутивы GNU/Linux используют программное обеспечение KDE или предоставляют возможность его установки. Полный список доступен на [KDE Community Wiki](https://community.kde.org/Distributions).

{% include back-button.html link="get" %}
