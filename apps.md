---
title: Приложения KDE
permalink: /apps
virtual-parent: /get
# SEO
image: img/apps.png
description: ! 'Сообщество KDE разрабатывает более 200 приложений: диспетчер файлов, текстовые и графические редакторы, браузер и офисный пакет, обучающие программы и многое другое.'
---

# Приложения KDE

![Иконки приложений](img/kirigami-devices.png){: .float-right style="width: 200px;"}
Сообщество KDE разрабатывает более двухсот приложений: диспетчер файлов, текстовые и графические редакторы, браузер и офисный пакет, обучающие программы и многое другое. Благодаря использованию кроссплатформенного фреймворка [Qt](https://www.qt.io) и основанных на нём библиотек [KDE Frameworks](https://develop.kde.org), наши программы работают на всех популярных операционных системах.  
[Полный список приложений](https://apps.kde.org/ru){: .learn-more .button}

## <i class="fab fa-linux pr-2" style="color: #7f8c8d;"></i> GNU/Linux и <i class="fab fa-freebsd px-2" style="color: #7f8c8d;"></i> FreeBSD

Все приложения KDE доступны для операционных систем GNU/Linux и FreeBSD. Для установки программ используйте центр приложений (например, [Discover](https://apps.kde.org/ru/discover/)), диспетчер пакетов вашего дистрибутива, [пакеты Flatpak](https://flathub.org/apps/search?q=kde){:target="_blank" rel="noreferrer"} или [Snap](https://snapcraft.io/publisher/kde){:target="_blank" rel="noreferrer"}.  
[Дистрибутивы GNU/Linux с приложениями KDE](distributions){: .learn-more .button}

## <i class="fab fa-windows pr-2" style="color: #7f8c8d;"></i> Windows

Для ОС Windows доступны стабильные выпуски следующих программ:

<div class="row">
<div class="col-md">
{% include app-link.html icon="kdeconnect.svg"     descr="Служба синхронизации KDE Connect"      get="https://kdeconnect.kde.org/download.html" web="https://apps.kde.org/ru/kdeconnect/" %}
{% include app-link.html icon="okular.svg" descr="Просмотрщик документов Okular" get="https://okular.kde.org/ru/download/" web="https://okular.kde.org/ru/" %}
{% include app-link.html icon="kate.svg"     descr="Мощный текстовый редактор Kate"         get="https://kate-editor.org/ru/get-it/#windows"                web="https://kate-editor.org/ru/" %}
{% include app-link.html icon="elisa.svg"     descr="Музыкальный проигрыватель Elisa"         get="https://apps.microsoft.com/detail/9pb5md7zh8tl?hl=RU-RU"                web="https://apps.kde.org/ru/elisa/" %}
{% include app-link.html icon="krita.svg"    descr="Графический редактор Krita"   get="https://krita.org/en/download/"   web="https://krita.org/" %}
{% include app-link.html icon="kdenlive.svg" descr="Видеоредактор Kdenlive"      get="https://kdenlive.org/en/download/"              web="https://kdenlive.org/" %}
{% include app-link.html icon="neochat.svg" descr="Matrix-клиент Neochat"      get="https://apps.kde.org/ru/neochat/"              web="https://apps.kde.org/ru/neochat/" %}
{% include app-link.html icon="kmymoney.svg"      descr="Менеджер личных финансов KMyMoney"      get="https://kmymoney.org/download.html"                           web="https://kmymoney.org" %}
{% include app-link.html icon="gcompris.svg" descr="Набор развивающих игр GCompris" get="https://gcompris.net/downloads-ru#windows" web="https://gcompris.net/index-ru" %}
{% include app-link.html icon="labplot.svg"     descr="Программа для анализа данных LabPlot"         get="https://labplot.kde.org/download/"         web="https://apps.kde.org/ru/labplot" %}
{% include app-link.html icon="crow.svg"     descr="Переводчик Crow Translate"         get="https://apps.kde.org/ru/crowtranslate/"         web="https://apps.kde.org/ru/crowtranslate/" %}
</div>

<div class="col-md">
{% include app-link.html icon="isoimagewriter.svg"  descr="Мастер записи образов ISO"         get="https://apps.kde.org/ru/isoimagewriter/"         web="https://apps.kde.org/ru/isoimagewriter/" %}
{% include app-link.html icon="kleopatra.svg"     descr="Диспетчер сертификатов Kleopatra"       get="https://www.gpg4win.org"         web="https://apps.kde.org/ru/kleopatra" %}
{% include app-link.html icon="kile.svg"     descr="Редактор TeX/LaTeX Kile"         get="https://www.microsoft.com/ru-ru/p/kile/9pmbng78pfk3"         web="https://kile.sourceforge.io/" %}
{% include app-link.html icon="filelight.svg" descr="Анализатор дисков Filelight" get="https://www.microsoft.com/ru-ru/p/filelight/9pfxcd722m2c" web="https://apps.kde.org/ru/filelight" %}
{% include app-link.html icon="kdevelop.svg" descr="Среда разработки KDevelop"              get="https://kdevelop.org/download#windows"              web="https://www.kdevelop.org/" %}
{% include app-link.html icon="umbrello.svg" descr="Редактор UML Umbrello"           get="https://download.kde.org/stable/umbrello/latest/" web="https://apps.kde.org/ru/umbrello/" %}
{% include app-link.html icon="rkward.svg"   descr="Интерфейс к языку R RKWard"      get="https://rkward.kde.org/RKWard_on_Windows.html"                   web="https://rkward.kde.org/" %}
{% include app-link.html icon="marble.svg"   descr="Виртуальный глобус Marble"       get="https://marble.kde.org/install.php"               web="https://marble.kde.org/" %}
{% include app-link.html icon="digikam.svg"  descr="Система управления фото digiKam" get="https://www.digikam.org/download/binary/#Windows" web="https://www.digikam.org/" %}
{% include app-link.html icon="kstars.svg" descr="Программа-планетарий KStars" get="https://kstars.kde.org/download/" web="https://kstars.kde.org" %}
</div>
</div>

*Windows® является зарегистрированной торговой маркой корпорации Microsoft в США и других странах.*

## <i class="fab fa-apple pr-2" style="color: #7f8c8d;"></i> macOS

Несколько проектов предоставляют бинарные сборки для macOS.  
*macOS является товарным знаком компании Apple Inc., зарегистрированным в США и других странах.*  
[Перейти к загрузке](https://community.kde.org/Mac){: .learn-more .button}

## <i class="fab fa-android pr-2" style="color: #7f8c8d;"></i> Android

Приложения KDE в Google Play:

<div class="row">
<div class="col-md">
{% include app-link.html icon="kdeconnect.svg"     descr="Служба синхронизации KDE Connect"      get="https://play.google.com/store/apps/details?id=org.kde.kdeconnect_tp" web="https://apps.kde.org/ru/kdeconnect/" %}
{% include app-link.html icon="gcompris.svg" descr="Набор развивающих игр GCompris" get="https://play.google.com/store/apps/details?id=net.gcompris.full" web="https://gcompris.net/index-ru" %}
</div>
<div class="col-md">
{% include app-link.html icon="itinerary.svg" descr="Помощник в путешествиях Маршруты KDE" get="https://play.google.com/store/apps/details?id=org.kde.itinerary" web="https://apps.kde.org/ru/itinerary/" %}
</div>
</div>

Некоторые другие приложения пока доступны только в [репозитории](https://community.kde.org/Android/F-Droid){:target="_blank" rel="noreferrer"} для [F-Droid](https://f-droid.org/ru/){:target="_blank" rel="noreferrer"}.

*Android является товарным знаком компании Google Inc. Google Play и логотип Google Play являются товарными знаками корпорации Google LLC.*

{% include back-button.html link="get" %}
