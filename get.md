---
title: Загрузить
permalink: /get
pre-content: plasma-desktop
# SEO
image: img/kirigami-devices.png
description: ! 'Скачать и установить KDE Plasma, приложения и другое программное обеспечение KDE'
---

## Приложения KDE

![Иконки приложений](img/apps.png){: .float-right}
Сообщество KDE разрабатывает более двухсот приложений: диспетчер файлов, текстовые и графические редакторы, браузер и офисный пакет, обучающие программы и многое другое.  
[Скачать приложения KDE](apps){: .learn-more .button .d-inline-block}

{% include app-carousel.html %}

## Plasma Mobile

![Смартфон с Plasma Mobile](img/plasmamobile.png){: .float-right width="177" height="250"}
Plasma Mobile — открытая мобильная графическая оболочка от KDE. Она поддерживает темы и виджеты Plasma и большинство программ для ОС Linux. Встроенные приложения построены с использованием библиотеки адаптивных компонентов интерфейса Kirigami.  
Plasma Mobile предустановлена на смартфон [PinePhone](https://pine64.org/devices/pinephone_pro/), а также может быть установлена на устройства с ОС Android, адаптированные проектом [postmarketOS](https://wiki.postmarketos.org/wiki/Devices). Работа через Halium [более не поддерживается](https://plasma-mobile.org/2020/12/14/plasma-mobile-technical-debt/).  
[Перейти на сайт проекта](https://www.plasma-mobile.org){: .learn-more .button .d-inline-block}
