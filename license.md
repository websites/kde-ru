---
title: Лицензия на материалы сайта
permalink: /license
additional-css: article
# SEO
description: ! 'Сведения об авторах материалов и правах на содержимое сайта'
---

# Лицензия на материалы сайта KDE.ru

Материалы сайта KDE.ru распространяются под лицензией [Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/legalcode.ru), если не указано иное.

Используются значки [Font Awesome](https://fontawesome.com/license/free).

## Авторы материалов сайта

Исходное содержимое сайта подготовили:

* [Илья Бизяев](https://ilyabiz.com)
* [Александр Поташев](mailto:aspotashev@gmail.com)
* [Григорий Мохин](mailto:mok@kde.ru)
* [Александр Наумов](mailto:alexander_naumov@opensuse.org)

При наличии в конкретных материалах иных данных об авторстве они имеют приоритет.

## Обратная связь

Для связи с администрацией сайта используйте [сообщения ВКонтакте](https://vk.me/kde_ru) или почтовый ящик <contact@kde.ru>.
