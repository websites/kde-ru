---
title: Введение в перевод
permalink: /translation
virtual-parent: /join
additional-css: article
# SEO
image: img/konqi/translator.png
description: ! 'Переводя программное обеспечение KDE на русский язык, вы делаете его доступнее и удобнее для русскоговорящих пользователей.'
---

# Введение в перевод на русский язык

Переводя программное обеспечение KDE на русский язык, вы делаете его доступнее и удобнее для русскоговорящих пользователей.

Эта статья рассказывает, как сделать свой вклад в локализацию программ и сайтов KDE, дополнив их перевод. Если вы нашли недочёт в существующем переводе, пожалуйста, сообщите о нём в [систему учёта ошибок](bugs) или в наш чат. Если же вам интересен перевод новостей и статей о KDE, см. статью о [продвижении](promotion).

## 1. Выбор объекта перевода

Прежде всего, зайдите на страницы статистики ([KDE Frameworks 5](https://l10n.kde.org/stats/gui/trunk-kf5/team/ru/), [KDE Frameworks 6](https://l10n.kde.org/stats/gui/trunk-kf6/team/ru/)) для ознакомления с состоянием перевода различных приложений KDE и документации к ним. В правой колонке показано текущее состояние локализации: *«зелёный»*{: style="color: green"} — утверждённый перевод, *«синий»*{: style="color: blue"} — черновой вариант, *«красный»*{: style="color: red"} — перевод отсутствует. Отсюда можно скачать файлы `.po` и `.pot` для перевода.

Если вы хотите исправить существующий перевод, используйте [сервис поиска по переводам](https://l10n.kde.org/dictionary/search-translations.php) для определения содержащего ошибку компонента.

**См. также:**

* [Перевод KDE UserBase](https://community.kde.org/RU/Перевод_KDE_UserBase)
* [Перевод зависимостей KDE](https://community.kde.org/RU/Перевод_зависимостей_KDE)

## 2. Подписка на рассылку

Почтовая рассылка является основным способом общения в команде локализации. Здесь вы можете получить ответ на интересующий вас вопрос по переводу, взаимодействию с командой или просто отправить на проверку готовый перевод. Обязательно [подпишитесь на рассылку](https://lists.kde.ru/mailman/listinfo/kde-russian). Перед тем как начать что-либо переводить, известите об этом других участников команды в рассылке.

## 3. Установка приложения для работы с переводами

Для перевода команда русской локализации использует приложение [Lokalize](https://apps.kde.org/ru/lokalize/). Можно использовать любое другое, например, Poedit.

**Подробнее:**

* [Работа с исходным кодом в Lokalize](https://community.kde.org/RU/Работа_с_исходным_кодом_в_Lokalize)
* [Современные инструменты локализации с точки зрения переводчика KDE](https://community.kde.org/RU/Современные_инструменты_локализации_с_точки_зрения_переводчика_KDE)

![Снимок экрана Lokalize](img/lokalize.png){:.img-fluid.mx-auto.d-block}

## 4. Работа с SVN

Многие переводчики KDE скачивают переводы всех программ, чтобы использовать переведённые строки в памяти переводов: это экономит ваш труд. Для получения этих файлов можно использовать `svn`:

```sh
svn co svn://anonsvn.kde.org/home/kde/trunk/l10n-kf5/ru/
```

— этой командой вы получите полный срез переводов находящихся в разработке программ;

```sh
svn co svn://anonsvn.kde.org/home/kde/trunk/l10n-kf5/templates/
```

— так вы получите срез шаблонов переводов разрабатываемой версии.

Вам может потребоваться заменить в командах `kf5` на `kf6`, если искомая программа уже перешла на платформу KDE Frameworks 6.

**Подробнее:**

* [Команды svn](https://svnbook.red-bean.com/index.ru.html)
* [Работа с переводами KDE через SVN](https://community.kde.org/RU/Работа_с_переводами_KDE_через_SVN)
* [Infrastructure/Subversion](https://community.kde.org/Infrastructure/Subversion)

## 5. Перевод

Основная часть работы — перевод — осуществляется с опорой на [Краткий справочник переводчика KDE](https://community.kde.org/RU/Краткий_справочник_переводчика_KDE). Рекомендуем внимательно прочитать этот документ перед началом работы, чтобы сэкономить время.

## 6. Отправка на проверку

Готовый перевод отправьте на проверку в рассылку. **Спасибо за ваш труд!**

{% include back-button.html link="join" %}
