---
title: Что умеет поиск в Plasma?
permalink: /kb/krunner
additional-css: article
# SEO
image: img/kb/thumbs/krunner.jpg
description: ! 'Полезные функции строки поиска и запуска KRunner'
---

# Что умеет поиск в Plasma?

В стандартную поставку рабочего окружения KDE Plasma входит KRunner — строка поиска и запуска с мощным встроенным функционалом и поддержкой сторонних расширений с дополнительными возможностями.

Для активации поисковой строки из любого приложения можно использовать глобальную комбинацию клавиш <kbd>Alt</kbd>+<kbd>Пробел</kbd> (или <kbd>Alt</kbd>+<kbd>F2</kbd>). Также KRunner откроется, если начать вводить текст на рабочем столе, а в новых версиях Plasma все возможности KRunner доступны при поиске в меню запуска приложений.

## Работа с файлами

### Поиск файлов

![KRunner, введено Wayland.pdf](/img/kb/screenshots/krunner/file-search.jpg){:.img-fluid.rounded}

При поиске учитываются недавно просмотренные файлы:

![KRunner, введено plasma_](/img/kb/screenshots/krunner/recent.jpg){:.img-fluid.rounded}

### Открытие расположений

![KRunner, введено ~/.local/share](/img/kb/screenshots/krunner/locations.jpg){:.img-fluid.rounded}

Можно указать название сохранённой точки входа:

![KRunner, введено Загрузки](/img/kb/screenshots/krunner/places.jpg){:.img-fluid.rounded}

## Управление программами

### Запуск приложений

![KRunner, введено Okular](/img/kb/screenshots/krunner/apps.jpg){:.img-fluid.rounded}

![KRunner, введено Офис](/img/kb/screenshots/krunner/app-office.jpg){:.img-fluid.rounded}

### Выполнение действий в приложениях

Некоторые приложения поддерживают выполнение действий, таких как создание файлов или переход к конкретным разделам интерфейса, напрямую из рабочего окружения:

![KRunner, введено инкогнито](/img/kb/screenshots/krunner/app-actions2.jpg){:.img-fluid.rounded}

![KRunner, введено библиотека](/img/kb/screenshots/krunner/app-actions.jpg){:.img-fluid.rounded}

### Запуск консольных команд

Введите консольную команду, которую хотите выполнить. По умолчанию команды запускаются в фоновом режиме, но, если команда интерактивная, вы можете выбрать запуск в окне эмулятора терминала.

![KRunner, введено zypper update](/img/kb/screenshots/krunner/shell.jpg){:.img-fluid.rounded}

### Открытие разделов Параметров системы

![KRunner, введено цвета](/img/kb/screenshots/krunner/settings.jpg){:.img-fluid.rounded}

Только из поиска доступен отладочный раздел «Отрисовка рабочего стола Plasma».

### Запуск профилей Konsole и сеансов Kate

Профили Konsole можно искать по названию:

![KRunner, введено SSH](/img/kb/screenshots/krunner/konsole-profile.jpg){:.img-fluid.rounded}

Для отображения сеансов Kate введите запрос «kate»:

![KRunner, введено kate](/img/kb/screenshots/krunner/kate-session.jpg){:.img-fluid.rounded}

### Завершение процессов

![KRunner, введено завершить firefox](/img/kb/screenshots/krunner/kill.jpg){:.img-fluid.rounded}

Можно выбрать между обычным (SIGTERM) и принудительным (SIGKILL) завершением.

## Управление средой

### Выбор окон

![KRunner, введено kas, найдено окно Kasts](/img/kb/screenshots/krunner/windows.jpg){:.img-fluid.rounded}

### Переключение рабочих столов

![KRunner, введено стол 2](/img/kb/screenshots/krunner/virtual-desktop.jpg){:.img-fluid.rounded}

### Управление сеансом

![KRunner, введено выключить](/img/kb/screenshots/krunner/session.jpg){:.img-fluid.rounded}

Поддерживаемые команды:

* «завершить сеанс»
* «заблокировать»
* «переключить» (смена сеанса)
* «выключить»
* «перезагрузить»
* «спящий режим» (или «на диск»)
* «ждущий режим» (также «в ОЗУ» или «приостановка»)

### Настройка яркости экрана

![KRunner, введено яркость экрана 30](/img/kb/screenshots/krunner/brightness.jpg){:.img-fluid.rounded}

![KRunner, введено затемнение экрана](/img/kb/screenshots/krunner/darken.jpg){:.img-fluid.rounded}

## Сеть

### Переход на вкладки веб-браузера

Для работы этого и следующего модулей необходима установленная утилита Plasma Browser Integration (фоновая служба и расширение для браузера). Возможно, при поиске KRunner сообщит о необходимости предоставить дополнительные разрешения.

![KRunner, введено руководство, найдена такая вкладка](/img/kb/screenshots/krunner/browser-tabs.jpg){:.img-fluid.rounded}

### Поиск по журналу посещённых сайтов

![KRunner, введено загрузить Kate, найдена такая страница в истории Firefox](/img/kb/screenshots/krunner/browser-history.jpg){:.img-fluid.rounded}

### Поиск в интернете

Для поиска в интернете можно добавить перед запросом сокращение, соответствующее поисковой системе, и двоеточие. Список таких сокращений можно посмотреть и изменить в Параметрах системы (раздел «Ключевые слова для веб-поиска»).

![KRunner, введено g:пушкин](/img/kb/screenshots/krunner/web-search.jpg){:.img-fluid.rounded}

### Открытие веб-сайтов

![KRunner, введено kde.ru](/img/kb/screenshots/krunner/web-url.jpg){:.img-fluid.rounded}

### Отправка e-mail

Если ввести адрес электронной почты, KRunner предложит отправить на него письмо:

![KRunner, введено ivanov@yandex.ru](/img/kb/screenshots/krunner/email-address.jpg){:.img-fluid.rounded}

При установленном наборе приложений Kontact возможна также отправка писем контактам:

![KRunner, введено иванов, найден такой контакт](/img/kb/screenshots/krunner/email-contact.jpg){:.img-fluid.rounded}

## Вычисления

### Калькулятор

Выражения для вычисления начинаются со знака `=` и задаются в синтаксисе библиотеки [Qalculate](https://github.com/Qalculate/libqalculate):

![KRunner, введено математическое выражение](/img/kb/screenshots/krunner/calc-basic.jpg){:.img-fluid.rounded}

Простые арифметические выражения можно вводить и без знака равенства.

### Решение уравнений, символьные вычисления

![KRunner, введено уравнение, обёрнутое в =solve()](/img/kb/screenshots/krunner/calc-solve.jpg){:.img-fluid.rounded}

### Перевод единиц измерения

![KRunner, введено 40 см, получены значения в других единицах](/img/kb/screenshots/krunner/convert-val.jpg){:.img-fluid.rounded}

![KRunner, введено 40 см в дюймах, получен ответ](/img/kb/screenshots/krunner/convert-val-to.jpg){:.img-fluid.rounded}

### Конвертер валют

![KRunner, введено 25 евро, получены суммы в других валютах](/img/kb/screenshots/krunner/convert-money.jpg){:.img-fluid.rounded}

![KRunner, KRunner, введено 25 евро в рублях, получен ответ](/img/kb/screenshots/krunner/convert-money-to.jpg){:.img-fluid.rounded}

## Справочные данные

### Проверка правописания

![KRunner, введено орфо клавиатура, ошибок нет](/img/kb/screenshots/krunner/spelling.jpg){:.img-fluid.rounded}

### Открытие страниц документации man

Введите `#` и название искомой страницы:

![KRunner, введено \#tar](/img/kb/screenshots/krunner/man.jpg){:.img-fluid.rounded}

### Символ Unicode по коду

Введите `#` и шестнадцатеричный код символа:

![KRunner, введено \#2248, получен символ ≈](/img/kb/screenshots/krunner/symbols.jpg){:.img-fluid.rounded}

### Словарные определения

Поддерживается только английский толковый словарь:

![KRunner, введено определение plasma, получены 3 словарных статьи](/img/kb/screenshots/krunner/dict.jpg){:.img-fluid.rounded}

Требуется подключение к сети.

### Дата и время

Для текущего либо указанного (на английском) часового пояса:

![KRunner, введено дата](/img/kb/screenshots/krunner/date.jpg){:.img-fluid.rounded}

![KRunner, введено время berlin](/img/kb/screenshots/krunner/time.jpg){:.img-fluid.rounded}

## Разработка

### Консоль сценариев

По запросам «desktop console» и «wm console» запускается консоль для ввода сценариев [Plasma](https://develop.kde.org/docs/plasma/scripting/) или [KWin](https://develop.kde.org/docs/plasma/kwin/):

![KRunner, введено desktop console](/img/kb/screenshots/krunner/dev-console.jpg){:.img-fluid.rounded}

### Отладочная консоль KWin

По запросу «kwin» KRunner предложит открыть отладочную консоль диспетчера окон:

![KRunner, введено kwin](/img/kb/screenshots/krunner/dev-kwin-debug.jpg){:.img-fluid.rounded}

## Управление модулями

Управление модулями поиска осуществляется в Параметрах системы: раздел *Поиск > Строка поиска*.

Там же можно настроить расположение KRunner на экране, очистить историю запросов и загрузить сторонние модули с дополнительными функциями. Многие из них требуют сборки из исходного кода: возможно, ваш дистрибутив предоставляет соответствующие пакеты.

![Сторонний модуль для запуска виртуальных машин](/img/kb/screenshots/krunner/3rdparty_vm.jpg){:.img-fluid.rounded}

*[Сторонний модуль](https://github.com/alvanieto/vbox-runner){:target="_blank" rel="noreferrer"} для запуска виртуальных машин*

{% include fixme.html %}

{% include back-button.html link="/kb" %}
