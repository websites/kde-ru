---
title: Продвинутая конфигурация
permalink: /kb/advanced-config
additional-css: article
# SEO
image: img/kb/thumbs/advanced_config.jpg
description: ! 'Параметры программного обеспечения KDE для продвинутых пользователей'
---

# Продвинутая конфигурация

Редактирование этих опций требует значительных знаний о работе с конфигурационными файлами и консольными командами. Будьте осторожны!

## Plasma

### Отключение декораций распахнутых окон

В файле `~/.config/kwinrc` в секции `[Windows]` нужно задать значение переменной `BorderlessMaximizedWindows=true`:

```bash
kwriteconfig5 --file kwinrc --group Windows --key BorderlessMaximizedWindows true
```

После этого перезапустите KWin, либо попросите его перечитать конфигурацию:

```bash
qdbus org.kde.KWin /KWin org.kde.KWin.reconfigure
```

{% include fixme.html %}

{% include back-button.html link="/kb" %}
