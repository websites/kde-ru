---
title: Установка и настройка Kvantum
permalink: /kb/kvantum
additional-css: article
# SEO
image: img/kb/thumbs/kvantum.jpg
description: ! 'Разбираем процесс установки самого движка Kvantum и тем для него'
---

# Установка и настройка Kvantum

> Обновлено: 30.05.2022

[Kvantum](https://github.com/tsujan/Kvantum) — это сторонний движок тем на основе векторной графики формата SVG для приложений, реализованных с использованием Qt Widgets. В этой статье мы разберём процесс установки самого движка и тем для него.

{% include alert-warning.html text="Разработчики KDE не гарантируют совместимость приложений с Kvantum. В частности, темы Kvantum могут не полностью применяться к современным приложениям, реализованным с применением QML и Kirigami." %}

## Установка

### Kubuntu, KDE Neon

Актуальная версия Kvantum для Ubuntu доступна в PPA команды Papirus:

```sh
sudo add-apt-repository ppa:papirus/papirus  
sudo apt update  
sudo apt install qt5-style-kvantum qt5-style-kvantum-themes
```

### openSUSE

Kvantum доступен в openSUSE Tumbleweed и в нескольких репозиториях сообщества для Leap:

* [software.opensuse.org/package/kvantum-qt5](https://software.opensuse.org/package/kvantum-qt5)
* [build.opensuse.org/project/show/KDE:Extra](https://build.opensuse.org/project/show/KDE:Extra)

### Arch

```sh
sudo pacman -S kvantum
```

### Fedora

```sh
sudo dnf install kvantum
```

## Использование

Kvantum можно настроить при помощи Диспетчера Kvantum, который поставляется вместе с самим движком тем:

![Диспетчер Kvantum](/img/kb/screenshots/kvantum/kvantum-manager.jpg){:.img-fluid.rounded}

Чтобы установить новую тему, выберите каталог, в который вы её скачали, и нажмите кнопку «Установить тему».  
Сделать тему активной можно во вкладке «Изменение или удаление тем»:

![Применение темы в Диспетчере Kvantum](/img/kb/screenshots/kvantum/apply-theme.jpg){:.img-fluid.rounded}

Просто выберите тему из списка и нажмите «Использовать тему».

Чтобы активировать тему, нужно выбрать Kvantum в Параметрах системы: *Внешний вид > Оформление приложений*, а также в разделе *Цвета* выбрать набор цветов устанавливаемой темы (с префиксом Kv):

![Активация оформления приложений Kvantum в Параметрах системы](/img/kb/screenshots/kvantum/syse-appstyle.jpg){:.img-fluid.rounded}

![Выбор цветовой схемы Kvantum в Параметрах системы](/img/kb/screenshots/kvantum/syse-colorscheme.jpg){:.img-fluid.rounded}

Готово, тема применена!

## Темы

Темы Kvantum можно найти в каталоге [KDE Store](https://store.kde.org/browse?cat=123). Вот некоторые примеры:

### [Otto](https://t.me/kde_ru_news/136)

![Снимок темы Otto](/img/kb/screenshots/kvantum/themes/otto.jpg){:.img-fluid.rounded}

### [Glassy](https://t.me/kde_ru_news/153)

![Снимок темы Glassy](/img/kb/screenshots/kvantum/themes/glassy.jpg){:.img-fluid.rounded}

### [Moe (Dark)](https://t.me/kde_ru_news/280)

![Снимок темы Moe Dark](/img/kb/screenshots/kvantum/themes/moe.jpg){:.img-fluid.rounded}

### [Sweet KDE](https://t.me/kde_ru_news/83)

![Снимок темы Sweet KDE](/img/kb/screenshots/kvantum/themes/sweet.jpg){:.img-fluid.rounded}

### [Arc](https://github.com/PapirusDevelopmentTeam/arc-kde)

![Снимок темы Arc](/img/kb/screenshots/kvantum/themes/arc.jpg){:.img-fluid.rounded}

### [Materia](https://github.com/PapirusDevelopmentTeam/materia-kde)

![Снимок темы Materia](/img/kb/screenshots/kvantum/themes/materia.jpg){:.img-fluid.rounded}

### [Breeze-Noir-Dark](https://t.me/kde_ru_news/254)

![Снимок темы Breeze-Noir-Dark](/img/kb/screenshots/kvantum/themes/breeze-noir.jpg){:.img-fluid.rounded}

### [Glow Dark](https://store.kde.org/p/1190427/)

![Снимок темы Glow Dark](/img/kb/screenshots/kvantum/themes/glow-dark.jpg){:.img-fluid.rounded}

### [DesktopPal97](https://t.me/kde_ru_news/736)

![Снимок темы DesktopPal97](/img/kb/screenshots/kvantum/themes/desktoppal97.jpg){:.img-fluid.rounded}

{% include fixme.html %}

{% include back-button.html link="/kb" %}
